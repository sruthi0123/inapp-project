
public class Shark extends Fish {
	private int length;

	public Shark(int fins, String tail, int teeth, int length) {
		super(fins, tail, teeth,"Shark");
		this.length = length;
	}
	
	

	@Override
	public void greeting() {
		super.greeting();
		System.out.println("And iam a shark and "+this.length+" long");
	}
	
	public void move_rate(int rate) {
		System.out.println("Shark is moving at the rate:"+rate+" kmph");
	}


	public int getLength() {
		return length;
	}
	
	

}
