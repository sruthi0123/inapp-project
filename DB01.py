import sqlite3
conn = sqlite3.connect('cars.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS CARS")
cursor.execute("""CREATE TABLE CARS(
                  CAR_NAME CHAR(50),
                  OWNER_NAME CHAR(50)
)""")
conn.commit()
query = "INSERT INTO CARS (CAR_NAME,OWNER_NAME) VALUES (?,?)"
cursor.execute(query, ('Audi','Asok'))
cursor.execute(query, ('Santro','Kelly'))
cursor.execute(query, ('Chevrolet','Jim'))
cursor.execute(query, ('Volkswagen','Hari'))
cursor.execute(query, ('Kia','Michel'))
cursor.execute(query, ('Toyota','Chandler'))
cursor.execute(query, ('Honda','Joey'))
cursor.execute(query, ('Mazda','Monica'))
cursor.execute(query, ('BMW','Dwight'))
cursor.execute(query, ('Bentley','Phoebe'))
cursor.execute("SELECT * FROM CARS")
print(cursor.fetchall())
conn.commit()
conn.close()