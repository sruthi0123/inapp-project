import java.util.*;
public class Practiceproblems1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			System.out.println("Enter a string");                 //1
			String one = scanner.nextLine();
			System.out.println("Enter a string");
			String two = scanner.nextLine();
			System.out.println("Enter a string");
			String three = scanner.nextLine();
			System.out.println(one);
			System.out.println(two);
			System.out.println(three);
			
			String str_x ;                                       //2
			System.out.println("Enter an integer: ");
			boolean hasInt = scanner.hasNextInt(); 
			if(hasInt) {
				int x = scanner.nextInt();
				str_x = Integer.toString(x);
				str_x+=45;
				System.out.println(str_x+", successful conversion.");
			} 
			else {
				System.out.println("Unsuccessfull");
			}
			scanner.nextLine();
			
			System.out.println(NumberDouble(3.678));                        //3
			
			System.out.println("Enter an integer: ");                       //4
			int a = scanner.nextInt();
			if(a%2==0) {
				System.out.println("GOOD");
			}else
				System.out.println("BAD");
			
			System.out.println("Enter the number of numbers to multiply: ");          //5
			int n = scanner.nextInt();
			int product=1;
			for(int i=1;i<=n;i++) {
				System.out.println("Enter your number: ");
				int c = scanner.nextInt();
				product*= c;
			}
			System.out.println("Product: "+product);
			
			System.out.println("Enter the number of numbers to sum: ");          //6
			int n1 = scanner.nextInt();
			int final_sum=0;
			for(int i=1;i<=n1;i++) {
				System.out.println("Enter your number: ");
				int c = scanner.nextInt();
				final_sum+= c;
			}
			System.out.println("Sum: "+final_sum);
			
			System.out.println("Enter the number to know size:");               //7
			int size = scanner.nextInt();
			if(size>1 && size<=100) {
				if(size>1 && size<=30) {
					System.out.println("SMALL");
				}
				if(size>=31 && size<=60) {
					System.out.println("MEDIUM");
				}
				if(size>=61 && size<100) {
					System.out.println("LARGE");
				}
			}else
				System.out.println("Enter a valid size.");
			
			System.out.println("Enter a string to print: ");                    //8
			String b = scanner.nextLine();
			scanner.nextLine();
			System.out.println("Enter the integer: ");
			int y = scanner.nextInt();
			if(b.length()<=10) {
				if(y>1&&y<50) {
					System.out.println(y);
					System.out.println(b);
				}else
					System.out.println("Enter a valid integer between 1 & 50");
			}else
				System.out.println("String length should be less than 10");
			
			System.out.println("Enter an integer to know weird or not weird: ");               //9  
			int w = scanner.nextInt();
			if(w%2==0) {
				System.out.println("Not-Weird");
			}else
				System.out.println("Weird");
			
			System.out.println("Enter an integer to know its multiplication table:");         //10
			int number = scanner.nextInt();
			for(int i=1;i<=10;i++) {
				System.out.println(number+"*"+i+"="+(number*i));
			}
			
			scanner.close();
		}
	
	public static String NumberDouble(double num) {     
		return "DOUBLE "+num;
	}
	
	
	}
	

