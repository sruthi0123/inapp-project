import java.util.*;
import java.util.stream.Collectors;

public class Section_1 {

	public static void main(String[] args) {
		List<String> list1 = new ArrayList<String>();
		list1.add("Car");
		list1.add("Bike");
		list1.add("Scooter");
		list1.add("Lorry");
		list1.add("Van");
		list1.add("Lorry");
		
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(20);
		list2.add(23);
		list2.add(20);
		list2.add(18);
		list2.add(18);
		list2.add(25);
		list2.add(28);
		list2.add(32);
		
		List<Integer> newList2 = list2.stream().distinct().collect(Collectors.toList());
		System.out.println("ArrayList2 with duplicates removed: "
                + newList2); 
		List<String> newList1 = list1.stream().distinct().collect(Collectors.toList());
		System.out.println("ArrayList1 with duplicates removed: "
                + newList1); 
		
		Collections.sort(newList2);
		System.out.println("Numbers sorted in the order:");
		for(Integer number:newList2) {
			System.out.println(number);
		}
		
		List<Integer> evenList = new ArrayList<Integer>();
		List<Integer> oddList = new ArrayList<Integer>();
		for(Integer number:newList2) {
			if(number%2 == 0 ) {
				evenList.add(number);
			}else {
				oddList.add(number);
			}
		}
		
		System.out.println("Even number lists:"+evenList);
		System.out.println("Odd number lists:"+oddList);
		
		
	}

}
