import java.util.Arrays;
import java.util.HashSet;

public class Section_2 {

	public static void main(String[] args) {
		String para = "The APIs of NIO were designed to provide access to the "
				+"low-level I/O operations of modern operating systems."
				+ " Although the APIs are themselves relatively high-level, "
				+ "the intent is to facilitate an implementation that"
				+ " can directly use the most efficient operations of the underlying platform.";
		String[] words = para.split(" ");
		HashSet<String> uniquewords = new HashSet<String>(Arrays.asList(words));
		int count = 0;
		for(String s : uniquewords) {
			count++;
			System.out.println(s);
		}
		System.out.println();
		System.out.println("Number of unique words: "+count);
		
		String[] uwarray = new String[uniquewords.size()];
		uniquewords.toArray(uwarray);
		System.out.println();
		System.out.println("Printing the number of occurences of each word :");
		answerQueries(words,uwarray);

	}
	static void search(String[]arr, String s)
    {
            int counter = 0;
            for (int j = 0; j < arr.length; j++)

                if (s.toLowerCase().equals(arr[j].toLowerCase()))
                    counter++;
 
           System.out.println(s+" : "+counter);
    }
	static void answerQueries(String[] arr, String q[])
    {
        for (int i=0;i<q.length; i++)
            search(arr, q[i]) ;
    }
 

}
