import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
public class Section_3 {

	public static void main(String[] args) throws Exception{
		
			 String data = readFileAsString("C:\\Users\\Public\\Java\\java_1.txt");
			 System.out.println(data);   
			 
			  String[] words = data.split(" ");
			  
			  String[] wordsToLower = new String[words.length];
			  for(int i=0;i<words.length;i++) {
				  wordsToLower[i] = words[i].toLowerCase();
			  } 
			  
			  HashSet<String> uniqueWords = new HashSet<String>(Arrays.asList(wordsToLower));
			  int count = 0;
			  System.out.println();
			  System.out.println("Printing unique words in the paragraph: ");
				for(String s : uniqueWords) {
					count++;
					System.out.println(s);
				}
				System.out.println();
				System.out.println("Number of Unique Words: "+count);
				
				String[] uwarray = new String[uniqueWords.size()];
				uniqueWords.toArray(uwarray);
				System.out.println();
				System.out.println("Printing the number of occurences of each word :");
				answerQueries(words,uwarray);
	}
	
	public static String readFileAsString(String fileName)throws Exception 
	  { 
	    String data = ""; 
	    data = new String(Files.readAllBytes(Paths.get(fileName))); 
	    return data; 
	  } 
	
	
	
	
	static void search(String[]arr, String s)
    {
            int counter = 0;
            for (int j = 0; j < arr.length; j++)

                if (s.toLowerCase().equals(arr[j].toLowerCase()))
                    counter++;
 
           System.out.println(s+" : "+counter);
    }
	static void answerQueries(String[] arr, String q[])
    {
        for (int i=0;i<q.length; i++)
            search(arr, q[i]) ;
    }

}
