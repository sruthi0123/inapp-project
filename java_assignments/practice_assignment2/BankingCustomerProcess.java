import java.util.*;

public class BankingCustomerProcess {
	public static void main(String[] args) {
		
		List<String> customerName=new ArrayList<String>();  
		 customerName.add("Manu");  
		 customerName.add("Ravi");  
		 customerName.add("Jim");  
		 customerName.add("Pammy"); 
		 for(String name:customerName)  
			  System.out.println("Customer Name: "+name); 
		 
		 Customer myCustomer = new Customer(customerName.get(0),"Savings",5000);
		 System.out.println(depositProcessing());
		 System.out.println("Amount Deposited:"+myCustomer.depositAmount(300));
		 
		 System.out.println(withdrawProcessing());
		 System.out.println("Amount Withdrawn:"+myCustomer.withdrawAmount(500));
	}
	public static String depositProcessing() {
		return "Deposit processing";
	}
	public static String withdrawProcessing() {
		return "Withdraw processing";
	}
}
