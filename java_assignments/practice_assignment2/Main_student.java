import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class Main_student {

	public static void main(String[] args) {
		ArrayList<String> student_Name = new ArrayList<String>();
        ArrayList<Integer> rollNo = new ArrayList<Integer>();
        SortedSet<String> S_name = new TreeSet<>(Comparator.reverseOrder());
        SortedSet<Integer> S_num = new TreeSet<>(Comparator.reverseOrder());
        Scanner sc = new Scanner(System.in);
        String name="";
        int roll=0;
        System.out.println("Enter the total number of students: ");
        int strength = sc.nextInt();
        for (int i=1;i<=strength;i++) {
            System.out.println("Please enter your name: ");
            name = sc.next();
            student_Name.add(name);
            S_name.add(name);
            sc.nextLine();
            System.out.println("Please enter your roll number: ");
            roll = sc.nextInt();
            rollNo.add(roll);
            S_num.add(roll);
             
		
		}
        System.out.println(student_Name);
		System.out.println(rollNo);
		Collections.sort(student_Name);
		Collections.sort(rollNo);
		System.out.println("Sorted student names: "+student_Name);
		System.out.println("Sorted student roll numbers: "+rollNo);
		System.out.println("Reversed stuent name list: "+S_name);
		System.out.println("Reversed student roll number: "+S_num);
		
		Student student = new Student(roll,name);
        
}
}
