
public interface iBanking {
	public int depositAmount(int amount);
	public int withdrawAmount(int amount);
}
