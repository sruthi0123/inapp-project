import java.util.HashMap;
import java.util.Map;

public class Wordcount_main {

	public static void main(String[] args) {
		String[] words = {"apple","orange","mango","apple","apple","mango","pineapple"};
		
			  Map<String, Integer> map = new HashMap<String, Integer> ();
			  for (String s:words) {
			    
			    if (!map.containsKey(s)) {  
			      map.put(s, 1);
			    }
			    else {
			      int count = map.get(s);
			      map.put(s, count + 1);
			    }
			  }
			  map.forEach((key, value) -> System.out.println("Word: "+key + ". No. of occurances: " + value));
			  
			}

}
