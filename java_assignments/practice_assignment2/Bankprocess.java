import java.util.ArrayList;
import java.util.List;

public class Bankprocess {
	public static void main(String[] args) {
	Bank Indian = new Bank("Indian Bank");
	
	List<Customer> customers =  new ArrayList<Customer>();
	Customer c1 = new Customer("Yash","Savings",3000);
	Customer c2 = new Customer("Mahesh","Salary",3000);
	Customer c3 = new Customer("Ramesh","Salary",3000);
	Customer c4 = new Customer("Suresh","NRI",3000);
	customers.add(c1);
	customers.add(c2);
	customers.add(c3);
	customers.add(c4);
	System.out.println(Indian.getBankName());
	for(Customer c:customers) {
		System.out.println(c.getCustomerName()+" "+c.getDepositType()+" "+c.getBaseAmount());
	}
	customers.remove(1);
	System.out.println("Modified list");
	System.out.println(Indian.getBankName());
	for(Customer c:customers) {
		System.out.println(c.getCustomerName()+" "+c.getDepositType()+" "+c.getBaseAmount());
	}
	}
}