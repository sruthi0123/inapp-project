import java.util.*;
public class Bank {
	private String bankName;
	
	public Bank(String bankName) {
		super();
		this.bankName = bankName;
	}

	List<Customer> customers =  new ArrayList<Customer>();

	public String getBankName() {
		return bankName;
	}
	
}
