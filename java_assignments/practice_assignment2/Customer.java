
public class Customer implements iBanking{
	private String customerName;
	private String depositType;
	private int baseAmount;
	public Customer(String customerName, String depositType, int baseAmount) {
		super();
		this.customerName = customerName;
		this.depositType = depositType;
		this.baseAmount = baseAmount;
	}
	
	public int depositAmount(int amount) {
		this.baseAmount+=amount;
		return this.baseAmount;
	}
	public int withdrawAmount(int amount) {
		this.baseAmount-=amount;
		return this.baseAmount;
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getDepositType() {
		return depositType;
	}

	public int getBaseAmount() {
		return baseAmount;
	}
	

}
