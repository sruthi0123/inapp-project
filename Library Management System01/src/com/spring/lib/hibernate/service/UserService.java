package com.spring.lib.hibernate.service;

import com.spring.lib.hibernate.UserDetails;

public interface UserService {
	public abstract UserDetails validateUserCredential(String user_name,	String password);
	//public abstract User_details validateUserCredential(User_details loginForm);
}
