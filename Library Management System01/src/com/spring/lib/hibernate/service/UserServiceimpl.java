package com.spring.lib.hibernate.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.spring.lib.hibernate.UserDetails;
import com.spring.lib.hibernate.dao.UserDAO;
import com.spring.lib.hibernate.UserDetails;

@Service("userService")
public class UserServiceimpl implements UserService {
	
	
	@Autowired
	private UserDAO UserDAO;
	
	
	public void setUserDAO(UserDAO UserDAO) {
		this.UserDAO = UserDAO;
	}
	
	public UserDAO getUserDAO() {
		return UserDAO;
	}

	@Override
	public UserDetails validateUserCredential(String user_name, String password) {
		
		UserDetails user = getUserDAO().getUserDetailsByNameAndPassword(user_name, password);
		return user;
	
	}
	
//	@Override
//	public User_details validateUserCredential(User_details loginForm) {
//		
//		
//		User_details user = getUserDAO().findByUname(loginForm);
//		return user;
//	}
//	

}
