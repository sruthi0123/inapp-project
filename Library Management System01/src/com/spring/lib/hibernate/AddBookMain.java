package com.spring.lib.hibernate;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.spring.lib.hibernate.BookDetails;

public class AddBookMain {

	public void add_db(String name, String Author, int Price,String Book_edition, String language,Boolean avaliblity)
	{
	BookDetails book = new BookDetails();
	book.setBook_name(name);
	book.setBook_author(Author);
	book.setBook_price(Price);
	book.setBook_edition(Book_edition);
	book.setLanguage(language);
	book.setAvailability(avaliblity);
	
	
	SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
	Session session = sessionfactory.openSession();
	session.beginTransaction();
	session.save(book);
	session.getTransaction().commit();
	session.close();
    book= null;
	}

}
