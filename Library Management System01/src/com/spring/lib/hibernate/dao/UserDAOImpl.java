package com.spring.lib.hibernate.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.spring.lib.hibernate.UserDetails;

@Transactional
@Repository("userDAO")
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	public SessionFactory getMy_sessionfactory() {
        return my_sessionfactory;
    }

    @Resource(name = "sessionFactory")
    public void setMy_sessionfactory(SessionFactory my_sessionfactory) {
        this.my_sessionfactory = my_sessionfactory;
    }

    private SessionFactory my_sessionfactory;
    
    
    
//    @SuppressWarnings("unchecked")
//	@Override
//    public User_details findByUname(User_details loginForm) {
//        System.out.println("Hi Dao " + loginForm.getUser_name());
//        System.out.println("password "+loginForm.getPassword());
//        System.out.println("Address "+loginForm.getAddress());
//        Session session = my_sessionfactory.getCurrentSession();
//        Query query =  session.getNamedQuery("User_details.findByUname");
//        query.setString("user_name", loginForm.getUser_name());
//        query.setString("password", loginForm.getPassword());
//        User_details loginObj = (User_details) query.uniqueResult();
//        return loginObj;
//    }   
    
    
  
	
	@SuppressWarnings("unchecked")
	@Override
	public UserDetails getUserDetailsByNameAndPassword(String user_name, String password) {
		DetachedCriteria detachedCriteria =  DetachedCriteria.forClass(UserDetails.class);
		detachedCriteria.add(Restrictions.eq("user_name", user_name));
		detachedCriteria.add(Restrictions.eq("password", password));
		List<UserDetails> findByCriteria = (List<UserDetails>) hibernateTemplate.findByCriteria(detachedCriteria);
		if(findByCriteria !=null && findByCriteria.size()>0)
		return findByCriteria.get(0);
		else
			return null;
	}
	

}
