package com.spring.lib.hibernate.dao;

import com.spring.lib.hibernate.UserDetails;

public interface UserDAO {
	
	public UserDetails getUserDetailsByNameAndPassword(String user_name,String password);
	// public User_details findByUname(User_details loginForm);
}
