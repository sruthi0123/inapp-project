package com.spring.lib.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.lib.hibernate.AddUserMain;
import com.spring.lib.hibernate.AddBookMain;

@Controller
public class UserFuctionsController {
	
	@RequestMapping("/searchbookhomeuser")
	public ModelAndView addbookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("search-book-homeuser","message",message);
	}
	
	@RequestMapping("/view_by_iduser")
	public ModelAndView search_book(HttpServletRequest request, HttpServletResponse response)
	{	
		BookDetails book = new BookDetails();
		int book_ID= Integer.parseInt(request.getParameter("book_ID"));
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		book= (BookDetails) session.get(BookDetails.class, book_ID);
		int book_id= book.getBook_ID();
		String book_name= book.getBook_name();
		String book_author= book.getBook_author();
		int price= book.getBook_price();
		ModelAndView mv= new ModelAndView();
		mv.setViewName("search-book-resultuser");
		mv.addObject("book_id",book_id);
		mv.addObject("book_name",book_name);
		mv.addObject("book_author", book_author);
		mv.addObject("book_price", price);	
		return mv;
	}
	
	@RequestMapping("/renewbookhome")
	public ModelAndView renewbookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("renew-book-home","message",message);
	}
	
	@RequestMapping("/renew_book")
	public ModelAndView issuebook(HttpServletRequest request, HttpServletResponse response) throws ParseException
	{
		
		ModelAndView mv= new ModelAndView();
		mv.setViewName("renew-book-confirmation");
		
		BookDetails book = new BookDetails();
		UserDetails user = new UserDetails();
		
		int User_ID= Integer.parseInt(request.getParameter("user_id"));
		int Book_ID= Integer.parseInt(request.getParameter("book_id"));
		
		String  date_of_issue_revise=(request.getParameter("revise_date_of_issue")).toString();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date_of_issue = sdf.parse(date_of_issue_revise);
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		
		user= (UserDetails) session.get(UserDetails.class, User_ID);
		String user_name= user.getUser_name();
		int book1_id= user.getBook1_ID();
		int book2_id= user.getBook2_ID();
		
		if(book1_id == Book_ID) {
			user.setBook1_issue(date_of_issue);
			session.save(user);
			session.getTransaction().commit();
			session.close();
			mv.addObject("issue_date",date_of_issue_revise);
			
			return mv;
			
		}else if(book2_id == Book_ID) {
			user.setBook2_issue(date_of_issue);
			session.save(user);
			session.getTransaction().commit();
			session.close();
			mv.addObject("issue_date",date_of_issue_revise);
			
			return mv;
			
		}else {
			mv.setViewName("renew-book-notavailable");
			return mv;
		}
	}
	
	
}
