from random import randrange
class Pet():
    boredom_dec = 3
    hunger_dec = 2
    boredom_max = 10
    hunger_max = 12
    food_max = 20
    fun_max = 20
    sounds = ['"Grr..","Mushh.."']

    def __init__(self,name,animal_type):
        self.name = name
        self.animal_type = animal_type
        self.hunger = randrange(self.hunger_max)
        self.boredom = randrange(self.boredom_max)
        self.sounds = self.sounds[:]

    def clock_tick(self):
        self.hunger += 1
        self.boredom += 1

    def mood(self):
        if self.hunger <= self.hunger_max and self.boredom <= self.boredom_max :
            return " Happy."
        elif self.hunger >= self.hunger_max:
            return " Hungry."
        else:
            return "Bored."

    def __str__(self):
        return "Iam " +self.name+ ". I feel " +self.mood()+ "."

    def reduce_hunger(self):
        self.hunger = max(0, self.hunger - self.hunger_dec)

    def reduce_boredom(self):
        self.boredom = max(0, self.boredom - self.boredom_dec)

    def teach(self,word):
        self.sounds.append(word)
        self.reduce_boredom()

    def hi(self):
        print(self.sounds[randrange(len(self.sounds))])
        self.reduce_boredom()

    def feed(self):
        self.reduce_hunger()
        if self.hunger >= self.hunger_max:
            print('Iam still hungry')
        elif self.hunger >= self.food_max:
            print('iam full')
        else:
            pass

    def play(self):
        self.reduce_boredom()
        if self.boredom >= self.boredom_max:
            print('Iam still bored')
        elif self.boredom >= self.fun_max:
            print('Iam happy')
        else:
            pass

def main():
    pet_list = {'Dog':['kelly','jimmy'],'Cat':['pamy'],}
    print(pet_list)
    pet_name = input('Choose a pet from the pet_list')
    pet_type = input('What type is your pet animal? ')
    my_pet = Pet(pet_name,pet_type)
    print("Hello! Iam " + my_pet.name + " and iam a " + my_pet.animal_type + "\n Press enter to start")
    choice = None
    while True:
        print('''
        INTERACT WITH YOUR PET
        1.Feed your pet
        2.Talk with your pet
        3.Teach your pet a new word
        4.Play with your pet
        5.Adopt a new pet
        6.To know the mood of your pet
        0.Quit
        ''')
        choice = input('Enter your choice: ')
        if choice == '0':
            print('You Quit')
            break
        elif choice == '1':
            my_pet.feed()
        elif choice == '2':
            my_pet.hi()
        elif choice == '3':
            new_word = input('Enter the word you need to teach your pet')
            my_pet.teach(new_word)
        elif choice == '4':
            my_pet.play()
        elif choice == '5':
            x = input('Enter the pet name you need to add')
            y = input('Enter the pet animal')
            if y == 'Dog':
                pet_list['Dog'].append(x)
            elif y == 'Cat':
                pet_list['Cat'].append(x)
            elif y not in pet_list.keys():
                pet_list[y] = x
            print(pet_list)
        elif choice == '6':
            print(my_pet)
        else:
            print('Sorry! Invalid option')

main()