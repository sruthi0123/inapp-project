from IPython.display import clear_output
def display_board(board):
    clear_output()
    print( board[7]+ '|' +board[8]+ '|' +board[9])
    print('------')
    print(board[4]+ '|' +board[5]+ '|' +board[6])
    print('------')
    print(board[1]+ '|' +board[2]+ '|' +board[3])

import random
player_input = ' '
c = ["X","O"]
comp_input = random.choice(c)
if comp_input == 'X':
    player_input = 'O'
else:
    player_input = 'X'

def win_check(board,mark):
    return ((board[7] == mark and board[8] == mark and board[9] == mark) or
           (board[4] == mark and board[5] == mark and board[6] == mark) or
           (board[1] == mark and board[2] == mark and board[3] == mark) or
           (board[7] == mark and board[4] == mark and board[1] == mark) or
           (board[8] == mark and board[5] == mark and board[2] == mark) or
           (board[9] == mark and board[6] == mark and board[3] == mark) or
           (board[7] == mark and board[5] == mark and board[3] == mark) or
           (board[9] == mark and board[5] == mark and board[1] == mark))


def choose_first():
    if random.randint(0,1) == 0:
        return 'player'
    else :
        return 'computer'

def space_check(board,position):
    return board[position] == ' '

def full_board_check(board):
    for i in range(1,10):
        if space_check(board,i):
            return False
        else:
            return True

def player_choice(board):
    position = 0
    while position not in [1,2,3,4,5,6,7,8,9] or not space_check(board,position):
        position = int(input('Choose your next position:(1-9)'))
    return  position

def replay():
    return input('Do you want to play again? Y or N').lower()


print('Welcome to Tic Tac Toe Game!')
while True:
    theBoard = [' ']*10
    turn = choose_first()
    r = 1
    winner = ' '
    thisdict = {}
    print(turn+ ' will go first.')
    play_game = input('Are you ready to play?y or n')
    if play_game.lower() == 'y':
        game_on = True
    else:
        game_on = False
    while r<=3:
        print('Round:',r)
        while game_on:
            if turn == 'player':
                display_board(theBoard)
                position = player_choice(theBoard)
                theBoard[position] = player_input
                if win_check(theBoard,player_input):
                    display_board(theBoard)
                    print('Congratulations! You Won!')
                    winner = 'player'
                    game_on = False
                else:
                    if full_board_check(theBoard):
                        display_board(theBoard)
                        print('Its a Tie')
                        break
                    else:
                        turn = 'computer'
            elif turn == 'computer':
                display_board(theBoard)
                p = [1,2,3,4,5,6,7,8,9]
                c_position = int(random.choice(p))
                theBoard[c_position] = comp_input
                if win_check(theBoard,comp_input):
                    display_board(theBoard)
                    print('Computer Won!')
                    winner = 'computer'
                    game_on = False
                else:
                    if full_board_check(theBoard):
                        display_board(theBoard)
                        print('Its a tie')
                        break
                    else:
                        turn = 'player'
            else:
                pass
            thisdict[r] = [comp_input,player_input,winner]
        r+=1
    if not replay():
        break

a = int(input('Enter the round for which you need the details:'))
print('player input:{}'.format(thisdict[a][1]))
print('computer input:{}'.format(thisdict[a][0]))
print('The winner is {}'.format(thisdict[a][2]))






