import random
r=1
winner=0
ppoints=0
cpoints=0
thisdict={}
while r<=10:
    print("Round",r)
    print("Enter user's choice")
    pchoice=input()
    c=["rock","paper","scissor"]
    if pchoice.lower() not in c: print("Enter a valid choice")
    cchoice=random.choice(c)
    if pchoice.lower() == cchoice.lower(): winner='Tie'
    if pchoice.lower() == "rock" and cchoice.lower() == "scissor": winner = 'player'
    if pchoice.lower() == "rock" and cchoice.lower() == "paper": winner = 'computer'
    if pchoice.lower() == "paper" and cchoice.lower() == "scissor": winner = 'computer'
    if pchoice.lower() == "paper" and cchoice.lower() == "rock": winner = 'player'
    if pchoice.lower() == "scissor" and cchoice.lower() == "rock": winner = 'computer'
    if pchoice.lower() == "scissor" and cchoice.lower() == "paper": winner = 'player'
    if winner=='player': ppoints=ppoints+1
    if winner=='computer': cpoints=cpoints+1
    thisdict[r] = [pchoice,cchoice,winner]
    r+=1
print(cpoints)
print(ppoints)
print('Enter the round for which you need the information:')
a=int(input())
print('Player choice={}' .format(thisdict[a][0]))
print('Computer choice={}'.format(thisdict[a][1]))
if winner == 'player': print('You won the game')
if winner == 'computer':print('Computer won the game')
if winner == 'Tie':print('The game was a tie')

