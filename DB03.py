import sqlite3
conn = sqlite3.connect('employee.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")
cursor.execute("""CREATE TABLE EMPLOYEE(
               Name CHAR(20) NOT NULL,
               Id INT NOT NULL,
               Salary INT,
               Department_id INT
)""")
conn.commit()
cursor.execute("ALTER TABLE EMPLOYEE ADD COLUMN City CHAR(20)")
query_1 = "INSERT INTO EMPLOYEE (Name, Id, Salary, Department_id, City) VALUES(?,?,?,?,?)"
cursor.execute(query_1, ('Sam', 101, 30000, 1, 'Trivandrum'))
cursor.execute(query_1, ('Lily', 102, 35000, 2, 'Kochi'))
cursor.execute(query_1, ('John', 103, 30000, 3, 'Podicherry'))
cursor.execute(query_1, ('Pam', 104, 50000, 4, 'Bangalore'))
cursor.execute(query_1, ('Kevin', 105, 45000, 5, 'Hyderabad'))
conn.commit()
cursor.execute("SELECT Name, Id, Salary FROM EMPLOYEE")
print(cursor.fetchall())
l = input("Enter the first letter of Employee Name: ")
cursor.execute("SELECT * FROM EMPLOYEE WHERE Name LIKE '{}%'".format(l))
print(cursor.fetchall())
i = input("Enter the employee id you need the details for: ")
cursor.execute("SELECT * FROM EMPLOYEE WHERE Id == {}".format(i))
print(cursor.fetchall())
x = input("Enter the employee id to change name: ")
new_name = input("Enter the new Name: ")
cursor.execute("UPDATE EMPLOYEE SET Name = '{}' WHERE Id == {}".format(new_name, x))
cursor.execute("SELECT Name FROM EMPLOYEE")
print(cursor.fetchall())


cursor.execute("DROP TABLE IF EXISTS DEPARTMENTS")
cursor.execute("""CREATE TABLE DEPARTMENTS(
                 Department_id INT NOT NULL,
                 Department_Name CHAR(20)
)""")
conn.commit()
query_2 = "INSERT INTO DEPARTMENTS (Department_id,Department_Name) VALUES (?,?)"
cursor.execute(query_2, (1,'Banking'))
cursor.execute(query_2, (2,'Issue'))
cursor.execute(query_2, (3,'Exchange Control'))
cursor.execute(query_2, (4,'Industrial Credit'))
cursor.execute(query_2, (5,'Expenditure'))
conn.commit()
y = input("Enter the Department id: ")
cursor.execute("""SELECT * FROM (EMPLOYEE E JOIN DEPARTMENTS D ON E.Department_id = D.Department_id)
                 WHERE D.Department_id == {}""".format(y))
print(cursor.fetchall())
conn.close()