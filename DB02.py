import sqlite3
conn = sqlite3.connect('hospital.db')
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS HOSPITAL")
cursor.execute(""" CREATE TABLE HOSPITAL(
                   Hospital_Id INT PRIMARY KEY NOT NULL,
                   Hospital_Name CHAR(50) NOT NULL,
                   Bed_Count INT
)""")
conn.commit()
query_1 = "INSERT INTO HOSPITAL (Hospital_Id,Hospital_Name,Bed_Count) VALUES(?,?,?)"
cursor.execute(query_1, (1, 'Mayo Clinic', 200))
cursor.execute(query_1, (2, 'Cleveland Clinic', 400))
cursor.execute(query_1, (3, 'Johns Hopkins', 1000))
cursor.execute(query_1, (4, 'UCLA Medical Center', 1500))
conn.commit()
cursor.execute("DROP TABLE IF EXISTS DOCTORS")
cursor.execute("""CREATE TABLE DOCTORS(
                Doctor_Id INT PRIMARY KEY NOT NUll,
                Doctor_Name CHAR(50) NOT NULL,
                Hospital_Id INT,
                Joining_Date CHAR,
                Speciality CHAR(50),
                Salary INT,
                Experience CHAR
)""")
conn.commit()
query_2 = "INSERT INTO DOCTORS (Doctor_Id,Doctor_Name,Hospital_Id,Joining_Date,Speciality,Salary,Experience) VALUES(?,?,?,?,?,?,?)"
cursor.execute(query_2,(101, 'David',1,'2005-02-10','Pediatric',40000,'NULL'))
cursor.execute(query_2,(102, 'Michael',1,'2018-07-23','Oncologist',20000,'NULL'))
cursor.execute(query_2,(103, 'Susan',2,'2016-05-19','Garnacologist',25000,'NULL'))
cursor.execute(query_2,(104, 'Robert',2,'2017-12-28','Pediatric',28000,'NULL'))
cursor.execute(query_2,(105, 'Linda',3,'2004-06-04','Garnacologist',42000,'NULL'))
cursor.execute(query_2,(106, 'William',3,'2012-09-11','Dermatologist',30000,'NULL'))
cursor.execute(query_2,(107, 'Richard',4,'2014-08-21','Garnacologist',32000,'NULL'))
cursor.execute(query_2,(108, 'Karen',4,'2011-10-17','Radiologist',30000,'NULL'))
conn.commit()
cursor.execute("SELECT Speciality FROM DOCTORS")
print(cursor.fetchall())
a = input('Enter the required Speciality of doctor: ')
b = int(input('Enter the Salary of doctor: '))
cursor.execute("SELECT Doctor_Name FROM DOCTORS where Speciality = '{}' and Salary >= '{}'".format(a,b))
print(cursor.fetchall())
def doc():
    c = int(input("Enter the hospital_id: "))
    cursor.execute("""SELECT D.Doctor_Name ,H.Hospital_Name FROM (DOCTORS D JOIN HOSPITAL H 
                  ON D.Hospital_Id = H.Hospital_Id) 
                  WHERE D.Hospital_Id={}""".format(c))

    print(cursor.fetchall())
doc()
conn.close()