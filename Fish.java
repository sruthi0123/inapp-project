
public class Fish {
	private int fins;
	private String tail;
	private int teeth;
	private String name;
	
	public Fish(int fins, String tail, int teeth, String name) {
		super();
		this.fins = fins;
		this.tail = tail;
		this.teeth = teeth;
		this.name = name;
	}
	
	public void move(int speed) {
		System.out.println("Fishes moves at the speed "+speed);
	}
	public void greeting() {
		System.out.println("Hello! iam a fish.");
	}

	public int getFins() {
		return fins;
	}

	public String getTail() {
		return tail;
	}

	public int getTeeth() {
		return teeth;
	}
	
	public String getName() {
		return name;
	}
	
	
	
}
