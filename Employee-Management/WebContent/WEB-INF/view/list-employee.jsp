<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Employees</title>

<!-- reference our style sheet -->
 <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css"  />

</head>
<body>

	<div id="wrapper">
	 <div id="header">
	 	<h2>ERM- Employee Relationship Manager</h2>
	 </div>
	</div>
           <div id="container">
           <div id="content">
           <!-- put a new buttom: add customer -->
           <input type="button" value="Add Employee" 
           		onclick="window.location.href='showFormForAdd'; return false;"
           		class="add-button"
           />
           
            <table>
            	<tr>
            	<th>First Name</th>
            	<th>Last Name</th>
            	<th>Email</th>
            	<th>Phone Number</th>
            	<th>Department</th>
            	<th>Action</th>
            	</tr>
            	
            	<c:forEach var="tempEmployee" items="${employers}">
            	
            	<!-- construct an update link with customer id -->
            	<c:url var="updateLink" value="/employer/showFormForUpdate">
            		<c:param name="employerId" value="${tempEmployee.id}" />
            	</c:url>
            	
            	<!-- construct an delete link with customer id -->
            	<c:url var="deleteLink" value="/employer/delete">
            		<c:param name="employerId" value="${tempEmployee.id}" />
            	</c:url>
            	
            	<c:url var="sendEmailLink" value="/Employee-Management/WebContent/WEB-INF/view/emailform.jsp">
            		<c:param name="employerId" value="${tempEmployee.id}" />
            	</c:url>
            	
            	<tr>
            		<td> ${tempEmployee.first_name} </td>
            		<td> ${tempEmployee.last_name} </td>
            		<td> <a href="${sendEmailLink}">${tempEmployee.email}</a></td>
            		<td> ${tempEmployee.phone_number} </td>
            		<td> ${tempEmployee.department} </td>
            		<td> <!-- display the update link -->
            			<a href="${updateLink}">Update</a>
            			|
            			<a href="${deleteLink}" onclick="if(!(confirm('Are you sure,you want to delete this customer?')))return false">Delete</a>
            		</td>
            	</tr>
            	</c:forEach>
            	
            </table>
           
           </div>
           
           </div>
</body>
</html>