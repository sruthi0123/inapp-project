package com.springdemo.service;

import java.util.List;

import com.springdemo.entity.Employee;

public interface EmployeeService {
	
	public List<Employee> getEmployees();

	public void saveEmployee(Employee theCustomer);

	public Employee getEmployee(int theId);

	public void deleteEmployee(int theId);
}
