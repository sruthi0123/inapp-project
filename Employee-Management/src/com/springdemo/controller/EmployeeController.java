package com.springdemo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springdemo.dao.EmployeeDAO;
import com.springdemo.entity.Employee;
import com.springdemo.service.EmployeeService;

@Controller
@RequestMapping("/employer")
public class EmployeeController {
	
	//need to inject the customer service
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping("/list")
	public String listEmployees(Model theModel) {
		
		//get customers from service
		List<Employee> theEmployees = employeeService.getEmployees();
		
		//add the customers to the model
		theModel.addAttribute("employers",theEmployees);
		
		return "list-employee";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		//create model attribute to bind form data
		Employee theEmployee = new Employee();
		theModel.addAttribute("employer", theEmployee);
		
		return "employee-form";
	}
	
	@PostMapping("/saveEmployee")
	public String saveCustomer(@ModelAttribute("employer") Employee theEmployee) {
		
		//save the employer using our service
		employeeService.saveEmployee(theEmployee);
		return "redirect:/employer/list";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("employerId") int theId,Model theModel) {
		
		//get the customer from the service
		Employee theEmployee = employeeService.getEmployee(theId);
		
		//set customer as a model attribute to pre-populate the form
		theModel.addAttribute("employer", theEmployee);
		
		//send over to our form
		
		return "employee-form";
	}
	
	@GetMapping("/delete")
	public String deleteEmployee(@RequestParam("employerId") int theId) {
		//delete the customer
		employeeService.deleteEmployee(theId);
		
		return "redirect:/employer/list";
	}
	
	@Autowired
    private JavaMailSender mailSender;
     
	@GetMapping("/sendEmail.do")
    @RequestMapping(method = RequestMethod.POST)
    public String doSendEmail(HttpServletRequest request) {
        // takes input from e-mail form
        String recipientAddress = request.getParameter("recipient");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");
         
        // prints debug info
        System.out.println("To: " + recipientAddress);
        System.out.println("Subject: " + subject);
        System.out.println("Message: " + message);
         
        // creates a simple e-mail object
        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message);
         
        // sends the e-mail
        mailSender.send(email);
         
        // forwards to the view named "Result"
        return "Result";
    }
	
}
