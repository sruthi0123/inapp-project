import sqlite3
conn = sqlite3.connect('database.sqlite')
cursor = conn.cursor()

cursor.execute("SELECT HomeTeam,FTHG, FTAG FROM MATCHES WHERE Season == 2010 AND HomeTeam =='Aachen' ORDER BY FTHG desc")
print(cursor.fetchall())

cursor.execute("SELECT COUNT(DISTINCT HomeTeam) FROM MATCHES WHERE Season ==2016 AND FTR =='H' GROUP BY HomeTeam ORDER BY FTHG desc")
print("total number of home games each team won during the 2016 season in descending order of number of home games: {}".format(cursor.fetchall()))

cursor.execute("SELECT * FROM UNIQUE_TEAMS LIMIT 10")
print('10 rows of unique_teams table: {}'.format(cursor.fetchall()))

cursor.execute("SELECT * FROM Teams_in_Matches , Unique_Teams WHERE Teams_in_Matches.Unique_Team_ID = Unique_Teams.Unique_Team_ID")
print(cursor.fetchall())

cursor.execute("SELECT * FROM Teams_in_Matches T JOIN Unique_Teams U ON T.Unique_Team_ID = U.Unique_Team_ID")
print(cursor.fetchall())

cursor.execute("SELECT * FROM Unique_Teams U JOIN Teams T ON U.TeamName = T.TeamName LIMIT 10")
print('10 rows of  Unique_Teams data table and the Teams table joined:{}'.format(cursor.fetchall()))

cursor.execute("SELECT  U.Unique_Team_ID, U.TeamName, T.AvgAgeHome, T.Season, T.ForeignPlayersHome FROM Unique_Teams U JOIN Teams T ON U.TeamName = T.TeamName LIMIT 5")
print(cursor.fetchall())

cursor.execute("""SELECT MAX(T.Match_ID),T.Unique_Team_ID,U.TeamName 
             FROM Teams_in_Matches T JOIN Unique_Teams U ON U.Unique_Team_ID=T.Unique_Team_ID
             WHERE (U.TeamName LIKE '%y') OR (U.TeamName LIKE '%r') GROUP BY T.Unique_Team_ID,TeamName""")
print(cursor.fetchall())
conn.commit()
conn.close()