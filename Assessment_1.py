class Pet():
    def __init__(self,species=None,name=""):
        self.species = species
        self.name = name

        if self.species != ('dog' or 'cat' or 'horse' or 'hamster'):
            print('Value error!')
        else:
            pass

    def __str__(self):
        if self.name == "":
            return "Species of: {},unnamed".format(self.species)
        else:
            return "Species of:{},named {}".format(self.species,self.name)
class Dog(Pet):
    def __init__(self,chases='cats'):
        Pet.__init__(self)
        self.chases = chases

    def __str__(self):
        if self.name == "":
            return "Species of dog,unnamed,chases {}".format(self.chases)
        else:
            return "Species of dog,named {} ,chases {}".format(self.name,self.chases)

class Cat(Pet):
    def __init__(self,hates='dogs'):
        Pet.__init__(self)
        self.hates = hates

    def __str__(self):
        if self.name == "":
            return "Species of cat,unnamed,hates {}".format(self.hates)
        else:
            return "Species of cat, named {},hates {}".format(self.name,self.hates)

class Main:
    my_pet = Pet('dog','pi')
    print(my_pet.__str__())
    my_dog = Dog()
    print(my_dog.__str__())
    my_cat = Cat()
    print(my_cat.__str__())







