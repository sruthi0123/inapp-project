package com.spring.lib.logincontrollers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.lib.hibernate.UserCredential;
//import com.spring.lib.hibernate.UserValidator;
import com.spring.lib.hibernate.User_details;
import com.spring.lib.hibernate.service.UserService;

@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public UserService getUserService() {
		return userService;
	}
	
	@RequestMapping("/admin")
	public ModelAndView admin(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("adminlogin","message",message);
	}

	
	@RequestMapping(value="/user",method=RequestMethod.GET)
	public String loginPage(Model model){
		model.addAttribute("userCredential", new UserCredential());
		return "userlogin";
	}
	
	@RequestMapping(value ="/hellouser" ,method=RequestMethod.POST)
	public ModelAndView loginSuccess(@Valid @ModelAttribute("userCredential") UserCredential userCredential,BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return new ModelAndView("userlogin");
		}
		
		ModelAndView modelAndView = new ModelAndView("hellopage_user");
		User_details user = getUserService().validateUserCredential(userCredential.getUser_name(), userCredential.getPassword());
		if(user!= null){
			modelAndView.addObject("user", user);
			return modelAndView;
		}else{
			 modelAndView = new ModelAndView("notFound");
		}
		return modelAndView;
	}
	

	   
	
	
	@RequestMapping("/hello")
	public ModelAndView hello(HttpServletRequest request, HttpServletResponse response)
	{
		String name = request.getParameter("name");
		String password = request.getParameter("pass");
		if(name.equals("admin") && password.equals("admin123")) {
			String message = "Welcome "+name;
			return new ModelAndView("hellopage_admin","message",message);
		} 
		else {
			return new ModelAndView("errorpage_admin","message","Invalid credentials!!!");
		}
			
	}
	
	@RequestMapping("/logout")
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("index","message",message);
	}
	
}
