package com.spring.lib.hibernate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="book_details")
public class Book_details {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="book_id")
	int Book_ID;
	@Column(name="book_name")
	String Book_name;
	@Column(name="book_author")
	String Book_author;
	@Column(name="book_edition")
	String Book_edition;
	@Column(name="book_price")
	int Book_price;
	@Column(name="language")
	String language;
	@Column(name="availability")
	Boolean  availability;
	
	

	public int getBook_ID() {
		return Book_ID;
	}
	public void setBook_ID(int book_ID) {
		Book_ID = book_ID;
	}
	public String getBook_name() {
		return Book_name;
	}
	public void setBook_name(String book_name) {
		Book_name = book_name;
	}
	public String getBook_author() {
		return Book_author;
	}
	public void setBook_author(String book_author) {
		Book_author = book_author;
	}
	public int getBook_price() {
		return Book_price;
	}
	public void setBook_price(int book_price) {
		Book_price = book_price;
	}


	public String getBook_edition() {
		return Book_edition;
	}
	public void setBook_edition(String book_edition) {
		Book_edition = book_edition;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public Boolean getAvailability() {
		return availability;
	}
	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}
}
