package com.spring.lib.hibernate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Table(name="user_details", schema="public")
public class User_details implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id")
	int User_ID;
	
	@NotEmpty
	@Column(name="user_name")
	String user_name;
	@Column(name="address")
	String Address;
	@Column(name="book1_id")
	int Book1_ID;
	@Column(name="book2_id")
	int Book2_ID;
	@Column(name="book1_issue")
	Date Book1_issue;
	@Column(name="book1_return")
	Date  Book1_return;
	@Column(name="book2_issue")
	Date Book2_issue;
	@Column(name="book2_return")
	Date  Book2_return;
	@Column(name="active")
	Boolean active;
	@NotEmpty
	@Column(name="password")
	String password;
	
	

	public int getUser_ID() {
		return User_ID;
	}
	public void setUser_ID(int user_ID) {
		User_ID = user_ID;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public int getBook1_ID() {
		return Book1_ID;
	}
	public void setBook1_ID(int book1_ID) {
		Book1_ID = book1_ID;
	}
	public int getBook2_ID() {
		return Book2_ID;
	}
	public void setBook2_ID(int book2_ID) {
		Book2_ID = book2_ID;
	}
	public Date getBook1_issue() {
		return Book1_issue;
	}
	public void setBook1_issue(Date book1_issue) {
		Book1_issue = book1_issue;
	}
	public Date getBook1_return() {
		return Book1_return;
	}
	public void setBook1_return(Date book1_return) {
		Book1_return = book1_return;
	}
	public Date getBook2_issue() {
		return Book2_issue;
	}
	public void setBook2_issue(Date book2_issue) {
		Book2_issue = book2_issue;
	}
	public Date getBook2_return() {
		return Book2_return;
	}
	public void setBook2_return(Date book2_return) {
		Book2_return = book2_return;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
