package com.spring.lib.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spring.lib.hibernate.AddUserMain;
import com.spring.lib.hibernate.AddBookMain;

@Controller
public class AdminFunctionsController {
	
	@RequestMapping("/adduserhome")
	public ModelAndView admin(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("add_user_home","message",message);
	}
	
	@RequestMapping("/add_user")
	public ModelAndView add_user(HttpServletRequest request, HttpServletResponse response) throws ParseException
	{
		String user_name= (request.getParameter("Name")).toString();
		String  Address=(request.getParameter("Address")).toString();
		int Book1_ID=0;
		int Book2_ID=0;
		Date Book1_issue= null;
		Date Book2_issue= null;
		Date Book1_return= null;
		Date Book2_return= null;
		String password= (request.getParameter("Password")).toString();
		
		AddUserMain add_user= new AddUserMain();
		add_user.add_user_db(user_name, Address, Book1_ID, Book1_issue, Book1_return, Book2_ID, Book2_issue, Book2_return, password);
			
		ModelAndView mv= new ModelAndView();
		mv.setViewName("add_user_confirmation");
		mv.addObject("user_name",user_name);
		mv.addObject("address",Address);
		mv.addObject("password", password);
		return mv;
		
	}
	
	@RequestMapping("/addbookhome")
	public ModelAndView addbookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("add_new_book","message",message);
	}
	
	@RequestMapping("/add_book")
	public ModelAndView add_book(HttpServletRequest request, HttpServletResponse response) throws ParseException
	{	
		String name= (request.getParameter("Name")).toString();
		String  author=(request.getParameter("Author")).toString();
		int Price= Integer.parseInt(request.getParameter("price"));			
		String  Book_edition=(request.getParameter("Book_edition")).toString();
		String  language=(request.getParameter("language")).toString();
		
		
		Boolean avalibility= true;
		
		AddBookMain addbook= new AddBookMain();
		addbook.add_db(name, author, Price,Book_edition, language, avalibility);
		ModelAndView mv= new ModelAndView();
		mv.setViewName("addbookconfirmation");
		mv.addObject("book_name",name);
		mv.addObject("book_author", author);
		mv.addObject("book_price", Price);
		mv.addObject("Book_edition", Book_edition);
		mv.addObject("language", language);		
		return mv;
	
}
	
	@RequestMapping("/issuebookhome")
	public ModelAndView issuebookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("issue_book_home","message",message);
	}
	
	@RequestMapping("/issue_book")
	public ModelAndView issuebook(HttpServletRequest request, HttpServletResponse response) throws ParseException
	{
		ModelAndView mv= new ModelAndView();
		mv.setViewName("issue_book_confirmation");
		ModelAndView mv_limit_excced= new ModelAndView();
		mv_limit_excced.setViewName("issue_book_limit");
		ModelAndView mv_book_not_avalible= new ModelAndView();
		mv_book_not_avalible.setViewName("book_not_avalible");
		Book_details book = new Book_details();
		User_details user = new User_details();
		
		int User_ID= Integer.parseInt(request.getParameter("user_id"));
		int Book_ID= Integer.parseInt(request.getParameter("book_id"));
		
		String  Date_of_issue_get=(request.getParameter("date_of_issue")).toString();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date_of_issue = sdf.parse(Date_of_issue_get);
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		
		user= (User_details) session.get(User_details.class, User_ID);
		String user_name= user.getUser_name();
		int book1_id= user.getBook1_ID();
		int book2_id= user.getBook2_ID();
		
		book= (Book_details) session.get(Book_details.class, Book_ID);
		String book_name= book.getBook_name();
		Boolean availability= book.getAvailability();
		if(availability== true)
		{
			if(book1_id ==0)
			{
				user.setBook1_ID(Book_ID);
				user.setBook1_issue(date_of_issue);
				book.setAvailability(false);
				session.save(user);
				session.save(book);
				session.getTransaction().commit();
				session.close();
				mv.addObject("user_id", User_ID);
				mv.addObject("book_id", Book_ID);
				mv.addObject("book_name", book_name);
				mv.addObject("username", user_name);
				mv.addObject("issue_date",Date_of_issue_get);
				
				return mv;
				
			}
			else if (book2_id ==0)
			{
				user.setBook2_ID(Book_ID);
				user.setBook2_issue(date_of_issue);
				book.setAvailability(false);
				session.save(user);
				session.save(book);
				session.getTransaction().commit();
				session.close();
				mv.addObject("user_id", User_ID);
				mv.addObject("book_id", Book_ID);
				mv.addObject("book_name", book_name);
				mv.addObject("username", user_name);
			
				return mv;
			}
			else
			{
				Boolean limit= true;
				mv_limit_excced.addObject("book_limit_exceed", limit);
				return mv_limit_excced;
			}
			
		}
		else
		{
			Boolean book_not_avalible= true;
			mv_book_not_avalible.addObject("book_not_avalible", book_not_avalible);
			return mv_book_not_avalible;
		}
		
		
	}
	
	@RequestMapping("/returnbookhome")
	public ModelAndView returnbookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("return_book_home","message",message);
	}
	
	@RequestMapping("/return_book")
	public ModelAndView returnbook(HttpServletRequest request, HttpServletResponse response) throws ParseException
	{
		long no_of_days=0;
		long fine=0;
		//normal confirmation
		ModelAndView mv= new ModelAndView();
		mv.setViewName("return_book_confirmation");
		// book not_avalible
		ModelAndView mv_book_ID_not_avalible= new ModelAndView();
		mv_book_ID_not_avalible.setViewName("return_book_notassigned_to_user");
		// error in date
		ModelAndView return_book_error_in_date= new ModelAndView();
		return_book_error_in_date.setViewName("return_book_error_in_date");	
		// wrong book_id
		ModelAndView return_book_wrong_book_id= new ModelAndView();	
		return_book_wrong_book_id.setViewName("return_book_wrong_book_id");
		
		Book_details book = new Book_details();
		User_details user = new User_details();
		
		int User_ID= Integer.parseInt(request.getParameter("user_id"));
		int Book_ID= Integer.parseInt(request.getParameter("book_id"));
		
		String  Date_of_return_get=(request.getParameter("date_of_return")).toString();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date_of_return = sdf.parse(Date_of_return_get);
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		
		user= (User_details) session.get(User_details.class, User_ID);
		String user_name= user.getUser_name();
		int book1_id= user.getBook1_ID();
		int book2_id= user.getBook2_ID();
		Date book1_issue= user.getBook1_issue();
		Date book2_issue= user.getBook2_issue();
		
		book= (Book_details) session.get(Book_details.class, Book_ID);
		String book_name= book.getBook_name();
		Boolean availability= book.getAvailability();
		
		if(availability == false)
		{
		
			if(Book_ID==book1_id)
			{
				//finding_no_of_days
				try
				{
				long diffrence= date_of_return.getTime()-book1_issue.getTime();
				 no_of_days= TimeUnit.DAYS.convert(diffrence, TimeUnit.MILLISECONDS);
				}
				catch(Exception e){
					 //error in calculating date sent to error page
				return	return_book_error_in_date;
					}
				
			    fine=fine_struture(no_of_days);  
							
				user.setBook1_ID(0);
				user.setBook1_issue(null);
				book.setAvailability(true);
				session.save(user);
				session.save(book);
				session.getTransaction().commit();
				session.close();
				mv.addObject("User_ID", User_ID);	
				mv.addObject("user_name", user_name);				
				mv.addObject("book_name", book_name);
				mv.addObject("book_id", book1_id);
				mv.addObject("book_issue", book1_issue);
				mv.addObject("book_return", date_of_return);
				mv.addObject("no_of_days", no_of_days);
				mv.addObject("fine", fine);
				return mv;
			}
			else if(Book_ID==book2_id )
			{
				user.setBook2_ID(0);
				user.setBook2_issue(null);
				book.setAvailability(true);
				session.save(user);
				session.save(book);
				session.getTransaction().commit();
				session.close();
				mv.addObject("User_ID", User_ID);	
				mv.addObject("user_name", user_name);
				mv.addObject("book_id", book2_id);
				mv.addObject("book_name", book_name);
				mv.addObject("book_issue", book2_issue);
				mv.addObject("book_return", date_of_return);
				mv.addObject("no_of_days", no_of_days);
				mv.addObject("fine", fine);
				return mv;
			}
			else
			{
				//wrong book_id
				return return_book_wrong_book_id;
				
			}
		}
		else{
			// book is not_avalible
			return mv_book_ID_not_avalible;
		}
	
	}
	public static long fine_struture(long no_of_days)
	{
		//fine_struture
		long fine=0;
		long fine_days;
		if(no_of_days<30)
		{
			fine=0;
		}
		else 
		{
		  fine_days= no_of_days-30;
		 fine= fine_days*2;
		}
		
		return fine;
	}
	
	@RequestMapping("/viewuserhome")
	public ModelAndView viewuserhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("view_user_home","message",message);
	}
	
	@RequestMapping("/view_user")
	public ModelAndView search_user(HttpServletRequest request, HttpServletResponse response)
	{	
		String book1_name=null;
		String book2_name=null;
		User_details user= new User_details();
		Book_details book = new Book_details();
		int user_ID= Integer.parseInt(request.getParameter("user_id"));
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		
		user= (User_details) session.get(User_details.class, user_ID);
		String user_name= user.getUser_name();
		int book1_id= user.getBook1_ID();
		int book2_id= user.getBook2_ID();
		Date book1_issue= user.getBook1_issue();
		Date book2_issue= user.getBook2_issue();
		
		if (book1_id !=0)
		{
		book= (Book_details) session.get(Book_details.class, book1_id);
		book1_name= book.getBook_name();
		}
		else if(book2_id !=0)
		{
			book= (Book_details) session.get(Book_details.class, book2_id);
			 book2_name= book.getBook_name();
		}
		else
		{
			
		}
		ModelAndView mv= new ModelAndView();
		mv.setViewName("view_user_result");
		
		mv.addObject("user_name",user_name);
		mv.addObject("book1_id",book1_id);
		mv.addObject("book1_name",book1_name);
		mv.addObject("book1_issue",book1_issue);
		
		mv.addObject("book2_id",book2_id);
		mv.addObject("book2_name",book2_name);
		mv.addObject("book2_issue",book2_issue);
		return mv;
		
	}
	

	@RequestMapping("/searchbookhome")
	public ModelAndView searchbookhome(HttpServletRequest request, HttpServletResponse response)
	{
		String message = "Welcome";
		return new ModelAndView("search_book_home","message",message);
	}
	
	@RequestMapping("/view_by_id")
	public ModelAndView search_book(HttpServletRequest request, HttpServletResponse response)
	{	
		Book_details book = new Book_details();
		int book_ID= Integer.parseInt(request.getParameter("book_ID"));
		
		SessionFactory sessionfactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionfactory.openSession();
		session= sessionfactory.openSession();
		session.beginTransaction();
		book= (Book_details) session.get(Book_details.class, book_ID);
		int book_id= book.getBook_ID();
		String book_name= book.getBook_name();
		String book_author= book.getBook_author();
		int price= book.getBook_price();
		ModelAndView mv= new ModelAndView();
		mv.setViewName("search_book_result");
		mv.addObject("book_id",book_id);
		mv.addObject("book_name",book_name);
		mv.addObject("book_author", book_author);
		mv.addObject("book_price", price);	
		return mv;
	}
	
}
