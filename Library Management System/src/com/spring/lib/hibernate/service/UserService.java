package com.spring.lib.hibernate.service;

import com.spring.lib.hibernate.User_details;

public interface UserService {
	public abstract User_details validateUserCredential(String user_name,	String password);
	//public abstract User_details validateUserCredential(User_details loginForm);
}
