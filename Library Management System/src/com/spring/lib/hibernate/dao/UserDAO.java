package com.spring.lib.hibernate.dao;

import com.spring.lib.hibernate.User_details;

public interface UserDAO {
	
	public User_details getUserDetailsByNameAndPassword(String user_name,String password);
	// public User_details findByUname(User_details loginForm);
}
