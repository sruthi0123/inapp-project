<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:useBean id="command" class="com.spring.lib.hibernate.User_details" scope="request"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User Login page</title>
<style type="text/css">
.error{
color:red;
}

</style>

</head>
<body bgcolor="#F9A7B0">
<br><br><br><br>
<center>
	<form:form action="hellouser" >
	<h4>User login Form</h4>
		<table>
			<tr><td>Name: <input type="text" name="Name"></td></tr>
			<tr>
				<td>Enter Username:</td>
				<td><form:input path="user_name"/></td>
				<td><form:errors path="user_name" cssClass="error"/></td>
			</tr>
			<tr>
				<td>Enter Password:</td>
				<td><form:password path="password"/></td>
				<td><form:errors path="password" cssClass="error"/></td>
			</tr>

			<tr>
				<td><input type="submit" value="Login"></td>
			</tr>
		</table>
	</form:form>
	</center>
</body>
</html>